import React, {useEffect} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';


const Splash =({navigation}) => {
    useEffect(() => {
     setTimeout (() => {
         navigation.replace('Form');
     },5000);
});
return (
    <View style={styles.wrapper}>
        <Image source={require('../assets/er.jpg')} style={{height:300,width:300}}/>
        <Text style={{fontSize:20,fontWeight:'bold',color:'green'}}>Tobat</Text>
    </View>     
    );
};

export default Splash;

const styles = StyleSheet.create({
    wrapper: {
        backgroundColor:'white',
        alignItems: 'center',
        justifyContent: 'center',
        flex :1,

    },
    logo:{
        margin: 10,
    },
});
