import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import React, { Component } from 'react'
import { Image,Text, StyleSheet, View, TouchableOpacity } from 'react-native'


export default class header extends Component {
  render() {
    return (
      <View>
       <Text style={{textAlign:'center',}}>بسم الله الرحمن الرحيم</Text>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 30,
    paddingTop: 20,
    
  },
  headerText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  underLine: {
    borderWidth: 1,

    marginTop: 10,
  },
});
