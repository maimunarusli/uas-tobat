import React, { Component } from 'react';

import {StyleSheet,Text,View,TextInput,TouchableOpacity} from 'react-native';
import Login from '../pages/login';

export default class Logo extends Component {
constructor(props){
    super(props);
    this.state = {};
}
render()
{
    return(
        
            <View style={styles.container}>
                <Login />
            <TextInput style={styles.inputBox}underlineColorAndroid='black'placeholder="Email"placeholderTextColor = "grey"selectionColor="black"

    keyboardType="email-address"onSubmitEditing={()=> this.password.focus()}

/>

<TextInput style={styles.inputBox}

underlineColorAndroid='black'

placeholder="Password"

secureTextEntry={true}

placeholderTextColor = "grey"

ref={(input) => this.password = input}

/>
<TouchableOpacity onPress={()=> this.props.navigation.navigate('header')}>
    <Text>
        login
    </Text>
</TouchableOpacity>
</View>

)

}

}

const styles = StyleSheet.create({

    container : {
    
    flexGrow: 1,
    
    justifyContent:'center',
    
    alignItems: 'center'
    
    },
    
    
    
    inputBox: {
    
    width:300,
    
    backgroundColor:'rgba(255, 255,255,0.2)',
    
    borderRadius: 25,
    
    paddingHorizontal:16,
    
    fontSize:16,
    
    color:'#ffffff',
    
    marginVertical: 10
    
    },
    
    button: {
    
    width:300,
    
    backgroundColor:'#1c313a',
    
    borderRadius: 25,
    
    marginVertical: 10,
    
    paddingVertical: 13
    
    },
    
    buttonText: {
    
    fontSize:16,
    
    fontWeight:'500',
    
    color:'#ffffff',
    
    textAlign:'center'
    
    }
    
    
    
    });
