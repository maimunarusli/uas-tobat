import React from 'react'
import {createStackNavigator} from '@react-navigation/stack'
import Splash from '../pages/splash';
import Form from '../pages/form';
import Header from '../pages/header';

const Stack =createStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
            />
            <Stack.Screen
            name="Form"
            component={Form}
            options={{headerShown: false}}
            />
             <Stack.Screen
            name="header"
            component={Header}
            options={{headerShown: false}}
            />
        </Stack.Navigator>
    );
};

export default Route;